﻿namespace USSDCommander
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nITray = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.лзщылкзещToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ыущлпызуплыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.щлаыщулаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ыыущлпзыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ыуопыоуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.щылупToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // nITray
            // 
            this.nITray.Text = "Информация о модеме";
            this.nITray.Visible = true;
            // 
            // cmsTray
            // 
            this.cmsTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.лзщылкзещToolStripMenuItem,
            this.ыущлпызуплыToolStripMenuItem});
            this.cmsTray.Name = "cmsTray";
            this.cmsTray.Size = new System.Drawing.Size(158, 70);
            // 
            // лзщылкзещToolStripMenuItem
            // 
            this.лзщылкзещToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.щлаыщулаToolStripMenuItem});
            this.лзщылкзещToolStripMenuItem.Name = "лзщылкзещToolStripMenuItem";
            this.лзщылкзещToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.лзщылкзещToolStripMenuItem.Text = "ыу";
            // 
            // ыущлпызуплыToolStripMenuItem
            // 
            this.ыущлпызуплыToolStripMenuItem.Name = "ыущлпызуплыToolStripMenuItem";
            this.ыущлпызуплыToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.ыущлпызуплыToolStripMenuItem.Text = "ыущлпызуплы";
            // 
            // щлаыщулаToolStripMenuItem
            // 
            this.щлаыщулаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ыыущлпзыToolStripMenuItem,
            this.ыуопыоуToolStripMenuItem,
            this.щылупToolStripMenuItem});
            this.щлаыщулаToolStripMenuItem.Name = "щлаыщулаToolStripMenuItem";
            this.щлаыщулаToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.щлаыщулаToolStripMenuItem.Text = "щлаыщула";
            // 
            // ыыущлпзыToolStripMenuItem
            // 
            this.ыыущлпзыToolStripMenuItem.Name = "ыыущлпзыToolStripMenuItem";
            this.ыыущлпзыToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ыыущлпзыToolStripMenuItem.Text = "ыыущлпзы";
            // 
            // ыуопыоуToolStripMenuItem
            // 
            this.ыуопыоуToolStripMenuItem.Name = "ыуопыоуToolStripMenuItem";
            this.ыуопыоуToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ыуопыоуToolStripMenuItem.Text = "ыуопыоу";
            // 
            // щылупToolStripMenuItem
            // 
            this.щылупToolStripMenuItem.Name = "щылупToolStripMenuItem";
            this.щылупToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.щылупToolStripMenuItem.Text = "щылуп";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 97);
            this.ControlBox = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "MainForm";
            this.cmsTray.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon nITray;
        private System.Windows.Forms.ContextMenuStrip cmsTray;
        private System.Windows.Forms.ToolStripMenuItem лзщылкзещToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem щлаыщулаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ыыущлпзыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ыуопыоуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ыущлпызуплыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem щылупToolStripMenuItem;
    }
}