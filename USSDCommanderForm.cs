﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace USSDCommander
{
    public partial class USSDCommanderForm : Form
    {
        ModemController Modem;
        

        public USSDCommanderForm()
        {
            InitializeComponent();
            tbOutputField.TextChanged += tbOutputField_TextChanged;
            foreach (string modemName in ModemController.GetModemList())
            {
                cbModemList.Items.Add(modemName);
            }
            cbModemList.SelectedIndex = 0;
            Modem = new ModemController();            
            Modem.Writer = new FormWriter(tbOutputField);
            Modem.onOpen += Modem_onOpen;
            Modem.onClosed += Modem_onClosed;
            
        }

        void tbOutputField_TextChanged(object sender, EventArgs e)
        {
            tbOutputField.SelectionStart = tbOutputField.Text.Length;
            tbOutputField.ScrollToCaret();
        }

        void Modem_onClosed()
        {
            gbUssd.Enabled = false;
            cbModemList.Enabled = true;
            bConnect.Text = "Connect";
            tbInputField.Clear();
            tbOutputField.Clear();
        }

        void Modem_onOpen()
        {
            gbUssd.Enabled = true;
            cbModemList.Enabled = false;
            bConnect.Text = "Disconnect";
            tbInputField.Focus();
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            Modem.SendUSSD(tbInputField.Text);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Modem.Dispose();
        }
        
        private void bConnect_Click(object sender, EventArgs e)
        {
            if (Modem.IsOpen)
            {
                Modem.Close();
            }
            else
            {
                string port = GetPortName(cbModemList.Text);
                if(port.Length > 0)
                    Modem.Open(port);
            }
        }
        private string GetPortName(string modemName)
        {
            Match match = Regex.Match(modemName, string.Format("{0}.+?{1}", Regex.Escape("("), Regex.Escape(")")));
            if (match.Success)
            {
                return match.Groups[0].Value.Trim('(', ')');
            }
            return "";
        }

        private void tbInputField_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                bSend.PerformClick();
        }
    }
}
