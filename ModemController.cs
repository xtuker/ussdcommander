﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;

namespace USSDCommander
{
    class ModemController : IDisposable
    {
        public delegate void PortStatus();
        public event PortStatus onOpen;
        public event PortStatus onClosed;

        SerialPort Port;
        public System.IO.TextWriter Writer = Console.Out;
        public System.IO.TextReader Reader = Console.In;
        public System.IO.TextWriter ErrorWriter = Console.Out;
        public bool IsOpen { get { return Port.IsOpen; } }
        public ModemController()
        {
            Port = new SerialPort();
            Port.DataReceived += Port_DataReceived;
            Port.ErrorReceived += Port_ErrorReceived;
            Port.Disposed += Port_Disposed;            
        }
        public bool Open(string portName)
        {
            Port.PortName = portName;
            Port.BaudRate = 9600;
            Port.Parity = Parity.None;
            Port.DataBits = 8;
            Port.StopBits = StopBits.One;
            Port.Encoding = Encoding.ASCII;
            Port.Open();          

            Send("AT");
            Send("ATE0");
            Send("AT+CMGF=1");
            Send("AT+CSCS=\"GSM\"");

            Port.ReadExisting();

            if (Port.IsOpen)
            {
                onOpen();
                return true;
            }
            else
                return false;
        }
        public void Close()
        {
            Dispose();
        }
        void Port_Disposed(object sender, EventArgs e)
        {
            onClosed();
        }
        string Encode(string str)
        {
            if (str.Contains('"') == false)
                return str;

            string[] partitalStr = str.Split('"');
            return string.Format("{0}\"{1}\"{2}", partitalStr[0], ConverTo7bitHex(partitalStr[1]), partitalStr[2]);
        }
        public string ConverTo7bitHex(string data)
        {
            StringBuilder sb = new StringBuilder();
            string empty = string.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                string bin = Convert.ToString((byte)data[i], 2).PadLeft(7, '0');
                if (i != 0 && i % 8 != 0)
                    sb.AppendFormat("{0:X2}",
                        Convert.ToByte(string.Concat(bin.Substring(7 - i % 8), empty), 2)
                    );
                
                empty = bin.Substring(0, 7 - i % 8);
                if (i == data.Length - 1 && empty != string.Empty)
                    sb.AppendFormat("{0:X2}", Convert.ToByte(empty, 2));
            }
            return sb.ToString();
        }
        string ConvertToString(string hex)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hex.Length; i += 4)
                sb.Append((char)Convert.ToInt16(hex.Substring(i, 4), 16));

            return sb.ToString();
        }
        string Decode(string str)
        {
            if (str.Contains('"') == false)
                return str;

            string[] partitalStr = str.Split('"');
            string hex = partitalStr[1];

            string txt = "";
            try
            {
                txt = ConvertToString(hex);
            }
            catch (Exception)
            {
            }
            return string.Format("{0}\"{1}\"{2}", partitalStr[0], txt, partitalStr[2]);
        }
        public string GetUSSDResponse(string data)
        {
            return ConvertToString(data);
        }
        

        public bool SendUSSD(string ussd)
        {
            return Send(string.Format(@"AT+CUSD=1,""{0}""", ussd), true);
        }

        public bool Send(string command, bool encode = false)
        {
            if (Port.IsOpen)
                if (encode)
                    Port.Write(string.Format("{0} \r\n", Encode(command)));
                else
                    Port.Write(string.Format("{0} \r\n", command));

            return Port.IsOpen;
        }

        void Port_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            ErrorWriter.Write(((SerialPort)sender).ReadExisting());
        }

        void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Match match = Regex.Match(((SerialPort)sender).ReadExisting(), "CUSD.*?\"(.+?)\"");
            if (match.Success)
            {
                Writer.Write(GetUSSDResponse(match.Groups[1].Value) + "\r\n\r\n");
            }
        }

        public void Dispose()
        {
            Port.Dispose();
        }

        public static string[] GetModemList()
        {
            List<string> ModemList = new List<string>();
            string query = "SELECT * FROM Win32_POTSModem";
            string[] ModemObjects = new string[250];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            foreach (ManagementObject obj in searcher.Get())
            {
                if (obj["Status"].ToString() == "OK")
                    ModemList.Add(obj["Name"].ToString() + "(" + obj["AttachedTo"].ToString() + ")");
            }
            return ModemList.ToArray();
        }
    }
}
