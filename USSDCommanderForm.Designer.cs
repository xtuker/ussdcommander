﻿namespace USSDCommander
{
    partial class USSDCommanderForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbUssd = new System.Windows.Forms.GroupBox();
            this.tbOutputField = new System.Windows.Forms.TextBox();
            this.tbInputField = new System.Windows.Forms.TextBox();
            this.bSend = new System.Windows.Forms.Button();
            this.gbModem = new System.Windows.Forms.GroupBox();
            this.bConnect = new System.Windows.Forms.Button();
            this.cbModemList = new System.Windows.Forms.ComboBox();
            this.gbUssd.SuspendLayout();
            this.gbModem.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbUssd
            // 
            this.gbUssd.Controls.Add(this.tbOutputField);
            this.gbUssd.Controls.Add(this.tbInputField);
            this.gbUssd.Controls.Add(this.bSend);
            this.gbUssd.Enabled = false;
            this.gbUssd.Location = new System.Drawing.Point(12, 75);
            this.gbUssd.Name = "gbUssd";
            this.gbUssd.Size = new System.Drawing.Size(435, 215);
            this.gbUssd.TabIndex = 0;
            this.gbUssd.TabStop = false;
            this.gbUssd.Text = "USSD";
            // 
            // tbOutputField
            // 
            this.tbOutputField.BackColor = System.Drawing.SystemColors.Window;
            this.tbOutputField.Location = new System.Drawing.Point(7, 45);
            this.tbOutputField.Multiline = true;
            this.tbOutputField.Name = "tbOutputField";
            this.tbOutputField.ReadOnly = true;
            this.tbOutputField.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbOutputField.Size = new System.Drawing.Size(422, 164);
            this.tbOutputField.TabIndex = 2;
            // 
            // tbInputField
            // 
            this.tbInputField.Location = new System.Drawing.Point(7, 19);
            this.tbInputField.Name = "tbInputField";
            this.tbInputField.Size = new System.Drawing.Size(341, 20);
            this.tbInputField.TabIndex = 1;
            this.tbInputField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInputField_KeyPress);
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(354, 17);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(75, 23);
            this.bSend.TabIndex = 0;
            this.bSend.Text = "Send";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // gbModem
            // 
            this.gbModem.Controls.Add(this.bConnect);
            this.gbModem.Controls.Add(this.cbModemList);
            this.gbModem.Location = new System.Drawing.Point(12, 13);
            this.gbModem.Name = "gbModem";
            this.gbModem.Size = new System.Drawing.Size(435, 56);
            this.gbModem.TabIndex = 1;
            this.gbModem.TabStop = false;
            this.gbModem.Text = "Modem";
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(354, 17);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(75, 23);
            this.bConnect.TabIndex = 1;
            this.bConnect.Text = "Connect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // cbModemList
            // 
            this.cbModemList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModemList.Location = new System.Drawing.Point(6, 19);
            this.cbModemList.Name = "cbModemList";
            this.cbModemList.Size = new System.Drawing.Size(342, 21);
            this.cbModemList.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 297);
            this.Controls.Add(this.gbModem);
            this.Controls.Add(this.gbUssd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "USSDCommander";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.gbUssd.ResumeLayout(false);
            this.gbUssd.PerformLayout();
            this.gbModem.ResumeLayout(false);
            this.ResumeLayout(false);
            this.Icon = Properties.Resources.MainIcon;

        }

        #endregion

        private System.Windows.Forms.GroupBox gbUssd;
        private System.Windows.Forms.TextBox tbOutputField;
        private System.Windows.Forms.TextBox tbInputField;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.GroupBox gbModem;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.ComboBox cbModemList;
    }
}

