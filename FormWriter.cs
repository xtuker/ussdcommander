﻿using System.IO;
using System.Text;
using System.Windows.Forms;
namespace USSDCommander
{
    class FormWriter : TextWriter
    {
        TextBox TextBoxEntry;

        public FormWriter(TextBox textBox)
        {
            TextBoxEntry = textBox;
        }

        public override void Write(string value)
        {
            MethodInvoker methodInvokerDelegate = delegate()
                { TextBoxEntry.Text += value; };

            if (TextBoxEntry.InvokeRequired)
                TextBoxEntry.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }

        public override Encoding Encoding
        {
            get { return Encoding.ASCII; }
        }
    }
}
