﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace USSDCommander
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            
            this.TopLevel = false;
            this.Enabled = false;

            nITray.Icon = Properties.Resources.MainIcon;
            nITray.ContextMenuStrip = cmsTray;
            nITray.MouseClick += nITray_MouseClick;
        }

        void nITray_MouseClick(object sender, MouseEventArgs e)
        {
            nITray.ContextMenuStrip.Show(Cursor.Position);
        }
    }
}
